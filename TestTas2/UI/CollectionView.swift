//
//  CollectionView.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import SwiftUI

struct CollectionView<Content: View, Model: Hashable>: View {
  private let columns: Int
  private var list: [[Model]] = []

  init(columns: Int, list: [Model], @ViewBuilder content: @escaping (Model) -> Content) {
    self.columns = columns
    self.content = content

    self.setupList(list)
  }

  var body: some View {
    ScrollView(.vertical) {
      VStack(alignment: .leading) {
        ForEach(0..<self.list.count, id: \.self) { i in
          HStack {
            ForEach(self.list[i], id: \.self) { model in
              self.content(model)
                .frame(width: UIScreen.main.bounds.size.width / CGFloat(columns) - 16)
            }
          }
        }
      }
    }
  }

  private let content: (Model) -> Content
}

extension CollectionView {
  fileprivate mutating func setupList(_ list: [Model]) {
    var column = 0
    var columnIndex = 0

    for object in list {
      if columnIndex < self.columns {
        if columnIndex == 0 {
          self.list.insert([object], at: column)
          columnIndex += 1
        } else {
          self.list[column].append(object)
          columnIndex += 1
        }
      } else {
        column += 1
        self.list.insert([object], at: column)
        columnIndex = 1
      }
    }
  }
}
