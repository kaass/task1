//
//  ContentView.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import SwiftUI

struct ContentView: View {
  @ObservedObject var model = SearchViewModel()

  var body: some View {
    VStack(alignment: .leading) {
      Text("Search photo")
        .font(.largeTitle)
        .padding(.horizontal, 8)

      SearchView(text: $model.text)
      switch model.information {
      case .loading:
        CenteredView {
          ActivityIndicator()
            .frame(width: 64, height: 64)
            .foregroundColor(.orange)
        }
      case .success(let model):
        if model.photo.isEmpty {
          CenteredView { Text("No Items").font(.largeTitle) }
        } else {
          CollectionView(columns: 2, list: model.photo) { model in
            PhotoView(model: model)
          }
          .padding(.horizontal, 8)
        }
      case .failure(let error):
        CenteredView { Text(error.localizedDescription) }
      }
    }
    .padding()

  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
