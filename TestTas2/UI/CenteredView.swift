//
//  CenteredView.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import SwiftUI

struct CenteredView<Content: View>: View {
  let content: Content

  var body: some View {
    Group {
      Spacer()
      HStack {
        Spacer()
        content
        Spacer()
      }
      Spacer()
    }
  }
}

extension CenteredView where Content: View {
  @inlinable init(@ViewBuilder content: () -> Content) {
    self.content = content()
  }
}
