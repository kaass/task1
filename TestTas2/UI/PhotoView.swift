//
//  PhotoView.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import SwiftUI

struct PhotoView: View {
  @ObservedObject var model = ContentViewModel()

  var body: some View {
    switch model.photo {
    case .loading:
      return AnyView(
        ActivityIndicator()
          .frame(width: 32, height: 32)
          .foregroundColor(.orange))
    case .success(let data):
      if let image = UIImage(data: data) {
        return AnyView(
          Image(uiImage: image)
            .resizable()
            .onTapGesture {
              let av = UIActivityViewController(activityItems: [image], applicationActivities: nil)
              UIApplication.shared.windows.first?.rootViewController?.present(
                av, animated: true, completion: nil)
            })
      }
      return AnyView(
        Image(systemName: "photo")
          .resizable())
    case .failure(let error):
      return AnyView(Text(error.localizedDescription))
    }
  }

  init(model: PhotoModel) {
    self.model.model = model
  }
}

struct PhotoView_Previews: PreviewProvider {
  static var previews: some View {
    PhotoView(model: PhotoModel.empty)
  }
}
