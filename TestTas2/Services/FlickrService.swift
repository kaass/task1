//
//  FlickrService.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import Foundation

final class FlickrService {
  private let session = URLSession(configuration: .default)
}

extension FlickrService: Searchable {
  func search(by query: String) -> AnyPublisher<FlickrModel, Never> {
    guard let request = contentRequest(by: query) else {  // 1
      return Just(FlickrModel.empty)
        .eraseToAnyPublisher()
    }

    return session.dataTaskPublisher(for: request)
      .subscribe(on: DispatchQueue.global())
      .map { $0.data }
      .decode(type: FlickrModel.self, decoder: JSONDecoder())
      .catch { error in Just(FlickrModel.empty) }
      .receive(on: RunLoop.main)
      .eraseToAnyPublisher()
  }

}

extension FlickrService {

  fileprivate func contentRequest(by query: String) -> URLRequest? {
    guard var urlComponents = URLComponents(string: Constants.baseUrl) else {
      return nil
    }

    var queryItems = Constants.arguments.flatMap { key, value -> [URLQueryItem] in
      if let strings = value as? [String] {
        return strings.map { URLQueryItem(name: key, value: $0) }
      } else {
        return [URLQueryItem(name: key, value: value as? String)]
      }
    }

    queryItems.append(URLQueryItem(name: "tags", value: query))
    urlComponents.queryItems = queryItems

    guard let url = urlComponents.url else {
      return nil
    }

    var request = URLRequest(url: url)
    request.httpMethod = "GET"

    return request
  }
}

extension FlickrService {
  fileprivate enum Constants {
    static let baseUrl = "https://api.flickr.com/services/rest"
    static let arguments: [String: Any] = [
      "Content-Type": "application/json",
      "api_key": "7b86a2100d2409afb252c502057404f8",
      "method": "flickr.photos.search",
      "format": "json",
      "nojsoncallback": "true",
      "extras": [
        "media",
        "url_sq",
        "url_m",
      ],
      "per_page": "20",
      "page": "1",
    ]
  }
}
