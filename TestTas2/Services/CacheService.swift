//
//  CacheService.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import Foundation

final class CacheService {
  private let cache = NSCache<NSString, AnyObject>()
}

extension CacheService: Cacheable {
  func content(by id: String) -> AnyPublisher<Data, Never> {
    guard let data = cache.value(forKey: id) as? Data else {
      return Just(Data())
        .eraseToAnyPublisher()
    }

    return Just(data)
      .eraseToAnyPublisher()
  }

  func save(_ data: Data, with id: String) {
    cache.setValue(data, forKey: id)
  }

}
