//
//  LoaderService.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import Foundation

final class LoaderService {
  private let session = URLSession(configuration: .default)
}

extension LoaderService: Loadable {
  func content(by id: String) -> AnyPublisher<Data, Never> {
    guard let parsedURL = URL(string: id) else {
      return Just(Data())
        .eraseToAnyPublisher()
    }

    return session.dataTaskPublisher(for: parsedURL)
      .subscribe(on: DispatchQueue.global())
      .map { $0.data }
      .catch { error in Just(Data()) }
      .receive(on: RunLoop.main)
      .eraseToAnyPublisher()
  }

}
