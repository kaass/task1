//
//  Cacheable.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Foundation

protocol Cacheable: Loadable {
  func save(_ data: Data, with id: String)
}
