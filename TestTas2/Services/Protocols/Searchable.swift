//
//  Searchable.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine

protocol Searchable {
  func search(by query: String) -> AnyPublisher<FlickrModel, Never>
}
