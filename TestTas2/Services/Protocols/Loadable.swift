//
//  Loadable.swift
//  TestTas2
//
//  Created by Konstantin D. on 20.08.2021.
//

import Combine
import Foundation

protocol Loadable {
  func content(by id: String) -> AnyPublisher<Data, Never>
}
