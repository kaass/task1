//
//  FlickrModel.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

struct FlickrModel: Decodable {
  private var photos: PhotosInfoModel

  var photo: [PhotoModel] {
    get { photos.photo }
    set { photos.photo = newValue }
  }

  static var empty: FlickrModel { FlickrModel(photos: PhotosInfoModel(photo: [])) }
}

struct PhotosInfoModel: Decodable {
  var photo: [PhotoModel]
}
