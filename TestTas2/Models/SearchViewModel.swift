//
//  SearchViewModel.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import Foundation

class SearchViewModel: ObservableObject {
  @Published var text: String = Constants.initValue
  @Published var information: LoadingStatus<FlickrModel> = .loading

  private let searchService: Searchable = FlickrService()

  init() {
    $text
      .debounce(for: 0.3, scheduler: RunLoop.main)
      .removeDuplicates()
      .subscribe(on: DispatchQueue.global())
      .flatMap { (text: String) -> AnyPublisher<FlickrModel, Never> in
        self.searchService.search(by: text)
      }
      .map { .success($0) }
      .receive(on: RunLoop.main)
      .assign(to: \.information, on: self)
      .store(in: &cancellableSet)
  }

  private var cancellableSet: Set<AnyCancellable> = []
}

extension SearchViewModel {
  enum Constants {
    static let initValue = "Keyword"
  }
}
