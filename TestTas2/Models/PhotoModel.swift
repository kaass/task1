//
//  PhotoModel.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

struct PhotoModel: Decodable, Identifiable, Hashable {
  static var empty: PhotoModel { PhotoModel(id: "", url: "", height: .zero, width: .zero) }

  enum CodingKeys: String, CodingKey {
    case id
    case url = "url_m"
    case height = "height_m"
    case width = "width_m"
  }
  var id: String
  var url: String
  var height: Int
  var width: Int
}
