//
//  LoadingStatus.swift
//  TestTas2
//
//  Created by Konstantin D. on 20.08.2021.
//

enum LoadingStatus<ResultType> {
  case loading
  case success(ResultType)
  case failure(Error)
}
