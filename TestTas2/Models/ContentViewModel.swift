//
//  ContentViewModel.swift
//  TestTas2
//
//  Created by Konstantin D. on 19.08.2021.
//

import Combine
import Foundation

final class ContentViewModel: ObservableObject {
  @Published var model: PhotoModel = PhotoModel.empty
  @Published var photo: LoadingStatus<Data> = .loading

  private let cacheService: Cacheable = CacheService()
  private let loaderService: Loadable = LoaderService()

  init() {
    $model
      .debounce(for: 0.3, scheduler: RunLoop.main)
      .removeDuplicates()
      .subscribe(on: DispatchQueue.global())
      .flatMap { (model: PhotoModel) -> AnyPublisher<Data, Never> in
        self.loaderService.content(by: model.url)
      }
      .map { .success($0) }
      .receive(on: RunLoop.main)
      .assign(to: \.photo, on: self)
      .store(in: &cancellableSet)
  }

  private var cancellableSet: Set<AnyCancellable> = []
}

extension ContentViewModel {
  fileprivate func download(by url: String) {

  }
}
